﻿
&НаКлиенте
Процедура СписокНоменклатурыЦенаПриИзменении(Элемент)
	
	Стр = Элементы.СписокНоменклатуры.ТекущиеДанные;
	Стр.Сумма = Стр.Количество * Стр.Цена;
	Объект.СуммаПоДокументу = Объект.СписокНоменклатуры.Итог("Сумма");  
	
КонецПроцедуры
